﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoseMenu : BaseWindowView
{
    [SerializeField] private TextMeshProUGUI _score;
    [SerializeField] private TextMeshProUGUI _scoreMax;
    [SerializeField] private TextMeshProUGUI _coinPassed;
    [SerializeField] private TextMeshProUGUI _soundText;

    [SerializeField] private GameObject _winTitle;

    private GameManager _gameManager;


    public override WindowType GetWindowType()
    {
        return WindowType.Lose;
    }

    public override void Init()
    {
        _gameManager = GameManager.instance;

        _score.text = _gameManager.GameScore.ToString();
        _scoreMax.text = _gameManager.MaxGameScore.ToString();
        _coinPassed.text = _gameManager.CoinPassed.ToString();

        _winTitle.SetActive(_gameManager.IsMaxScoreHigher);

        if (PlayerPrefsWrapper.GetBool(SoundsManager.SoundKey, true))
        {
            _soundText.text = "Sound on";
        }
        else
        {
            _soundText.text = "Sound off";
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene("Main");
    }

    public void ChangeSound()
    {
        var isOn = PlayerPrefsWrapper.GetBool(SoundsManager.SoundKey, true);
        if (isOn)
        {
            _soundText.text = "Sound off";
            PlayerPrefsWrapper.SetBool(SoundsManager.SoundKey, false);
        }
        else
        {
            _soundText.text = "Sound on";
            PlayerPrefsWrapper.SetBool(SoundsManager.SoundKey, true);
        }
    }

    public override void Destroy()
    {

    }
}