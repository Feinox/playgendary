﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseWindowView : MonoBehaviour
{
    public abstract WindowType GetWindowType();
    public abstract void Init();

    public abstract void Destroy();
}



public enum WindowType
{
    Menu,
    Game,
    Lose
}
