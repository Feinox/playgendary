﻿using System;
using TMPro;
using UnityEngine;

public class GameWindow : BaseWindowView
{
    [SerializeField] private TextMeshProUGUI _score;
    [SerializeField] private TextMeshProUGUI _scoreMax;
    [SerializeField] private TextMeshProUGUI _coinPassed;

    private GameManager _gameManager;

    public override void Init()
    {
        _gameManager = GameManager.instance;

        _gameManager.OnGameScoreChange += SetScore;
        _gameManager.OnMaxGameScoreChange += SetMaxScore;
        _gameManager.OnCoinPassed += SetCoinPassed;

        _score.text = _gameManager.GameScore.ToString();
        _scoreMax.text = _gameManager.MaxGameScore.ToString();
        _coinPassed.text = _gameManager.CoinPassed.ToString();

        _gameManager.StartGame();
    }

    public override void Destroy()
    {
        if (_gameManager.OnGameScoreChange != null)
        {
            _gameManager.OnGameScoreChange -= SetScore;
        }

        if (_gameManager.OnMaxGameScoreChange != null)
        {
            _gameManager.OnMaxGameScoreChange -= SetMaxScore;
        }

        if (_gameManager.OnCoinPassed != null)
        {
            _gameManager.OnCoinPassed -= SetCoinPassed;
        }
    }

    private void SetScore(int score)
    {
        _score.text = score.ToString();
    }

    private void SetMaxScore(int max)
    {
        _scoreMax.text = max.ToString();
    }

    public void SetCoinPassed(int coinPassed)
    {
        _coinPassed.text = coinPassed.ToString();
    }

    public override WindowType GetWindowType()
    {
        return WindowType.Game;
    }
}