﻿using UnityEngine;

public class MainMenu : BaseWindowView
{
    private void Update()
    {
        if (Input.anyKeyDown)
        {
            UImanager.instance.SetScreen(WindowType.Game);
            GameManager.instance.IsGamePause = false;
        }
    }

    public override WindowType GetWindowType()
    {
        return WindowType.Menu;
    }

    public override void Init()
    {
        GameManager.instance.IsGamePause = true;
    }

    public override void Destroy()
    {

    }
}