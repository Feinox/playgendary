﻿using System;
using UnityEngine;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;

public class GameManager : MonoBehaviour
{
    #region Variables

    public float heightDistance = 1f; 
    private float _gameScore = 0;
    private int _maxScore;
    private float _gameTime = 0;
    private int _coinPassed = 0;

    public static GameManager instance = null;

    public Action<int> OnGameScoreChange;
    public Action<int> OnMaxGameScoreChange;
    public Action<int> OnCoinPassed;

    
    
    #endregion


    #region Properties

    public bool IsGamePause { get; set; } = false;
    public int GameScore => (int)_gameScore;
    public int MaxGameScore => _maxScore;
    public int CoinPassed => _coinPassed;

    public bool IsMaxScoreHigher { get; private set; }

    #endregion


    #region Unity lifecycle

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;                                        // create singleton
        }

        _maxScore = PlayerPrefs.GetInt(GamesConst.MaxScoreKey, 0);
        _coinPassed = PlayerPrefs.GetInt(GamesConst.Coinscount, 0);
        UImanager.instance.SetScreen(WindowType.Menu);
    }

    #endregion


    #region Public methods

    public void StartGame()
    {
        _gameScore = 0;
        _coinPassed = 0;
        _maxScore = PlayerPrefs.GetInt(GamesConst.MaxScoreKey, 0);
        _coinPassed = PlayerPrefs.GetInt(GamesConst.Coinscount, 0);

        IsGamePause = false;
    }

    public void LoseGame()
    {
        IsGamePause = true;

        UImanager uiManager = UImanager.instance;
        uiManager.SetScreen(WindowType.Lose);
        SoundsManager.Instance.PlayFirst(SoundsType.Lose);
    }
    public void AddGameScore(int value)
    {
        _gameScore += value;
        if (_gameScore > _maxScore)
        {
            _maxScore = (int)_gameScore;
            IsMaxScoreHigher = true;
            PlayerPrefs.SetInt(GamesConst.MaxScoreKey, _maxScore);
            OnMaxGameScoreChange?.Invoke(_maxScore);
        }

        OnGameScoreChange?.Invoke(GameScore);
    }

    public void SetCoinPassed()
    {
        _coinPassed++;
        PlayerPrefs.SetInt(GamesConst.Coinscount, _coinPassed);
        OnCoinPassed?.Invoke(_coinPassed);
    }
}
#endregion