﻿using System.Collections;
using System.Collections.Generic;
using MarchingBytes;
using UnityEngine;

public class MovingObjectsSpawner : MonoBehaviour
{
    [SerializeField] private string poolObjectName;
    [SerializeField] private Transform spawnPos;
    [SerializeField] private float minSeparationTime;
    [SerializeField] private float maxSeparationTime;
    [SerializeField] private bool isRightSide;

    private EasyObjectPool easyObjectPool;
    private void Start()
    {
        easyObjectPool = EasyObjectPool.instance;
        StartCoroutine(SpawnVehicle());  
    }

    public IEnumerator SpawnVehicle()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(minSeparationTime, maxSeparationTime));
            GameObject go = easyObjectPool.GetObjectFromPool(poolObjectName, spawnPos.position, Quaternion.identity);
            var obj = go.GetComponent<MovingObject>();
            if (obj != null)
            {
                obj.StartMove(isRightSide);
            }
            if (!isRightSide)
            {
                go.transform.Rotate(new Vector3(0, 180, 0));
            }
        }
    }
}