﻿using System.Collections;
using System.Collections.Generic;
using MarchingBytes;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    [SerializeField] private int minDistanceFromPlayer;
    [SerializeField] private int maxTerrainCount;
    [SerializeField] private List<string> terrainDatas = new List<string>();
    [SerializeField] private Transform terrainHolder;

    public List<GameObject> currentTerrains = new List<GameObject>();

    private Vector3 pos = new Vector3(0, 0, 0);
    public Vector3 currentPosition
    {
        get => pos;
        set => pos = value;
    }

    private void Start()
    {
        for (int i = 0; i < maxTerrainCount; i++)
        {
            GameObject terrain;
            if (i < 5)
            {
                terrain = EasyObjectPool.instance.GetObjectFromPool(terrainDatas[Random.Range(0, 2)], currentPosition,
                    Quaternion.identity, terrainHolder);
            }
            else if (i < 10)
            {
                terrain = EasyObjectPool.instance.GetObjectFromPool(terrainDatas[Random.Range(2, 4)], currentPosition,
                    Quaternion.identity, terrainHolder);
            }
            else
            {
                terrain = EasyObjectPool.instance.GetObjectFromPool(terrainDatas[Random.Range(0, 5)], currentPosition,
                    Quaternion.identity, terrainHolder);
            }
            SpawnTerrain(true, new Vector3(0, 0, 0), terrain);
        }

        maxTerrainCount = currentTerrains.Count;
    }
    
    public void TerrainSpawner(bool isStart, Vector3 playerPos)
    {
        if (currentPosition.x - playerPos.x < minDistanceFromPlayer || isStart)
        {
            var  terrain = EasyObjectPool.instance.GetObjectFromPool(terrainDatas[Random.Range(2, terrainDatas.Count)], currentPosition, 
                Quaternion.identity, terrainHolder);
            currentTerrains.Add(terrain);
            if (!isStart)
            {
                if (currentTerrains.Count > maxTerrainCount)
                {
                    Destroy(currentTerrains[0]);
                    currentTerrains.RemoveAt(0);
                }
            }

            currentPosition += Vector3.right;
        }
    }

    private void SpawnTerrain(bool isStart, Vector3 playerPos, GameObject terrain)
    {
        if (currentPosition.x - playerPos.x < minDistanceFromPlayer || isStart)
        {
            currentTerrains.Add(terrain);
            if (!isStart)
            {
                if (currentTerrains.Count > maxTerrainCount)
                {
                    Destroy(currentTerrains[0]);
                    currentTerrains.RemoveAt(0);
                }
            }

            currentPosition += Vector3.right;
        }
    }
}