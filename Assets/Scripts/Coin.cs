﻿using UnityEngine;

public class Coin : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        gameObject.SetActive(false);
        SoundsManager.Instance.PlayFirst(SoundsType.GetCoin);
        GameManager.instance.SetCoinPassed();
    }
}
