﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UImanager : MonoBehaviour
{
    [SerializeField] private List<BaseWindowView> _windowViews;

    public static UImanager instance = null;

    private BaseWindowView _currentScreen;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            // DontDestroyOnLoad(this);
        }
    }

    public void SetScreen(WindowType type)
    {
        if (_currentScreen != null)
        {
            _currentScreen.Destroy();
            _currentScreen.gameObject.SetActive(false);
        }

        foreach (var window in _windowViews)
        {
            if (window.GetWindowType() == type)
            {
                _currentScreen = window;
                window.gameObject.SetActive(true);
                window.Init();

                break;
            }
        }
    }
}
