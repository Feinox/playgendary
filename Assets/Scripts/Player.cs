﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using MarchingBytes;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private TerrainGenerator terrainGenerator;

    private bool isHopping;
    private Vector3 moveDir;
    private float jump;

    private int counterVertical = 0;
    private int counterMovie = 0;
    private int counterHorizontal = 0;

    private int startDelta = 0;
    private int finishDelta = 0;
    
    private bool isStayOnLog = false;
    
    private  Dictionary<string,List<int> > countries = new Dictionary<string, List<int>>();
    private List<int> obstacle = new List<int>();
 

    private void Start()
    {
        countries.Add("Grass", new List<int> { 4, 6, 9, -4, -7});
        countries.Add("Grass2", new List<int> { 2, 6, 9, -2, -5, -7});
        countries.Add("Grass3", new List<int> { 4, 7, 9, -1, -5, -7});
    }

    void Update()
    {        
        if (isStayOnLog)
        {
            finishDelta = (int) transform.parent.localPosition.z;
        }
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        var isMovingObj = collision.collider.GetComponent<MovingObject>();
        if (isMovingObj != null)
        {
            if (isMovingObj.isLog)
            {
                isStayOnLog = true;
                transform.parent = collision.collider.transform;
                startDelta = (int) transform.parent.localPosition.z;
            }
        }
        else
        {
            finishDelta = 0;
            isStayOnLog = false;
            transform.parent = null;
        }
    }

    public void MoveCharacter(Vector3 difference)
    {
        if (isHopping)
        {
            return;
        }

        moveDir = difference;
        isHopping = true;
        var isMove = true;

        var tempCounterVertical = counterVertical;
        var tempCounterHorizontal = counterHorizontal + startDelta - finishDelta;
        var tempCounterMovie = counterMovie;

        if (moveDir.x == 1)
        {
            terrainGenerator.TerrainSpawner(false, transform.position);
            tempCounterMovie++;
            if (counterVertical <= 7) //const max terrain - visibal
            {
                tempCounterVertical++;
            }
        }
        else if (moveDir.x == -1 && counterVertical >= 0)
        {
            tempCounterMovie--;
            tempCounterVertical--;
        }
        else
        {
            tempCounterHorizontal -= (int)moveDir.z;
        }
        
        var poolName = terrainGenerator.currentTerrains[tempCounterVertical].GetComponent<PoolObject>().poolName;
        var isFind = countries.TryGetValue(poolName, out obstacle);
        if (isFind)
        {
            foreach (var ob in obstacle)
            {
                if (tempCounterHorizontal == ob)
                {
                    isMove = false;
                    break;
                }
            }
        }

        if (isMove)
        {
            counterVertical = tempCounterVertical;
            counterHorizontal = tempCounterHorizontal;
            counterMovie = tempCounterMovie;
            
            var moveVector = new Vector3(counterMovie , 1 ,  -counterHorizontal);
            transform.DOJump(moveVector, 0.3f, 1, 0.4f).OnComplete(FinishHop);
            startDelta = 0;
            finishDelta = 0;
        }
        else
        {
            FinishHop();
        }
    }

    private void FinishHop()
    {
        isHopping = false;
    }
}


