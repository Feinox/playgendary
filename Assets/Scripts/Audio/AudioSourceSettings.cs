﻿using UnityEngine;


[CreateAssetMenu(fileName = "AudioSourceSettings", menuName = "NotGameData/AudioSourceSettings")]
public class AudioSourceSettings : ScriptableObject
{
    #region Variables

    [SerializeField] private SoundsTypelDictionary sounds;

    #endregion

    
    #region Properties
    public SoundsTypelDictionary Sounds => sounds;

    #endregion
}