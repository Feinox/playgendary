using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AudioClips
{
    public List<AudioClip> AudioClip;
}