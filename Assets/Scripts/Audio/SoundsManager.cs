﻿using UnityEngine;

public class SoundsManager : MonoBehaviour
{
    #region Variables

    public const string SoundKey = "Sound";
    
    [SerializeField] private AudioSourceSettings _audioSourceSettings;
    private AudioSource _audioSource;

    public static SoundsManager Instance;
    
    #endregion

    #region Unity lifecycle

    private void Awake()
    {
        _audioSource = gameObject.AddComponent<AudioSource>();
        if (Instance == null)
        {
            Instance = this;
        }
    }

    #endregion


    #region Public methods

    public void PlayFirst(SoundsType type)
    {
        if (!IsSoundEnabled())
        {
            return;
        }

        var sources = _audioSourceSettings.Sounds[type];
        if (sources != null)
        {
            _audioSource.PlayOneShot(sources.AudioClip[0]);
        }
    }

    public void PlayWithIndex(SoundsType type, int index)
    {
        if (!IsSoundEnabled())
        {
            return;
        }

        var sources = _audioSourceSettings.Sounds[type];
        if (sources != null)
        {
            if (index < sources.AudioClip.Count)
            {
                _audioSource.PlayOneShot(sources.AudioClip[index]);
            }
        }
    }

    #endregion


    #region Private methods

    private bool IsSoundEnabled()
    {
        return PlayerPrefsWrapper.GetBool(SoundKey, true);
    }

    #endregion
}

public enum SoundsType
{
    GetCoin = 0,
    Lose,
}