﻿using UnityEngine;

public static class PlayerPrefsWrapper 
{
    public static void SetBool(string key, bool state)
    {
        PlayerPrefs.SetInt(key, state ? 1 : 0);
    }

    public static bool GetBool(string key, bool defaultValue)
    {
        var value = defaultValue ? 1 : 0;
        return PlayerPrefs.GetInt(key, value) != 0;
    }
}