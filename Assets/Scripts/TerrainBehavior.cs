﻿using System;
using UnityEngine;

public class TerrainBehavior : MonoBehaviour
{
    [SerializeField] private GameObject[] coins;
    
    private void OnEnable()
    {
        foreach (var coin in coins)
        {
            coin.SetActive(true);
        }
    }
}
