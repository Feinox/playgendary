﻿using DG.Tweening;
using MarchingBytes;
using UnityEngine;

public class MovingObject : MonoBehaviour
{
    public bool isLog;
    // public bool isRight;

    public void StartMove(bool isRight)
    {
        transform.DOMoveZ(  isRight ? 20 : -20, 15).OnComplete(ReturnToPool).SetEase(Ease.Linear);
    }
    

    private void ReturnToPool()
    {
        EasyObjectPool.instance.ReturnObjectToPool(gameObject);
    }
}
